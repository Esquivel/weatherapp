package com.example.weatherapp.api;

import com.example.weatherapp.domain.DataDto;
import io.reactivex.Single;
import retrofit2.http.GET;
import retrofit2.http.Query;

public interface ForecastApi {

    @GET("data/2.5/forecast")
    Single<DataDto> get(
        @Query("id") final String location,
        @Query("appid") final String apiKey,
        @Query("lang") final String lang);

}
