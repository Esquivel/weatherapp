package com.example.weatherapp;

import com.example.weatherapp.api.ForecastApi;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;

public class Provider {

    private ForecastApi api;

    public ForecastApi provideForecastApi() {
        if (api == null) {
            final Retrofit retrofit = new Retrofit.Builder()
                .baseUrl("https://samples.openweathermap.org/")
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .addConverterFactory(GsonConverterFactory.create())
                .build();

            api = retrofit.create(ForecastApi.class);
        }

        return api;
    }
}