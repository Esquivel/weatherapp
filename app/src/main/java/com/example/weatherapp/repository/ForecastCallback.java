package com.example.weatherapp.repository;

import com.example.weatherapp.domain.ForecastDto;
import java.util.List;

public interface ForecastCallback {

    void onSuccess(List<ForecastDto> forecastList);

    void onError();

}
