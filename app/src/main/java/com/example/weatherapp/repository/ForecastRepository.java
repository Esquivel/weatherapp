package com.example.weatherapp.repository;

import androidx.annotation.NonNull;
import com.example.weatherapp.api.ForecastApi;
import com.example.weatherapp.domain.DataDto;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;
import java.util.ArrayList;

public class ForecastRepository {

    private static final String API_KEY = "b64e815d280be21d39d4dee771eca0f5";
    private static final String LANG = "sp, es";

    private final ForecastApi api;
    private DataDto dataDto;

    private Disposable disposable;
    private ForecastCallback callback;

    public ForecastRepository(final ForecastApi api) {
        this.api = api;
    }

    public void getForecast(@NonNull final String location) {
        if (dataDto != null && callback != null) {
            onSuccess(dataDto);
            return;
        }

        if (disposable != null && !disposable.isDisposed()) {
            disposable.dispose();
        }

        disposable = api.get("524901", API_KEY, LANG)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe(this::onSuccess, this::onFailed);
    }

    private void onSuccess(final DataDto dataDto) {
        this.dataDto = dataDto;

        if (callback != null) {
            callback.onSuccess(new ArrayList<>(dataDto.getForecastDtoList()));
        }
    }

    private void onFailed(final Throwable throwable) {
        if (callback != null) {
            callback.onError();
        }
    }

    public void setCallback(final ForecastCallback callback) {
        this.callback = callback;
    }

    public void dispose() {
        disposable.dispose();
    }
}