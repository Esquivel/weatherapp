package com.example.weatherapp.domain;

import com.google.gson.annotations.SerializedName;

public class MainDto {

    private static final double TO_CELSIUS = 273.15;

    private float temp;
    @SerializedName("temp_min")
    private float tempMin;
    @SerializedName("temp_max")
    private float tempMax;

    public int getTemp() {
        return (int) (temp - TO_CELSIUS);
    }

    public void setTemp(final float temp) {
        this.temp = temp;
    }

    public int getTempMin() {
        return (int) (tempMin - TO_CELSIUS);
    }

    public void setTempMin(final float tempMin) {
        this.tempMin = tempMin;
    }

    public int getTempMax() {
        return (int) (tempMax - TO_CELSIUS);
    }

    public void setTempMax(final float tempMax) {
        this.tempMax = tempMax;
    }
}
