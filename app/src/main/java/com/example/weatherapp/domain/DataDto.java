package com.example.weatherapp.domain;

import com.google.gson.annotations.SerializedName;
import java.util.List;

public class DataDto {

    @SerializedName("list")
    private List<ForecastDto> forecastDtoList;

    public List<ForecastDto> getForecastDtoList() {
        return forecastDtoList;
    }

    public void setForecastDtoList(final List<ForecastDto> forecastDtoList) {
        this.forecastDtoList = forecastDtoList;
    }
}