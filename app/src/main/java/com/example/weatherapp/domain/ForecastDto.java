package com.example.weatherapp.domain;

import com.google.gson.annotations.SerializedName;
import java.util.List;

public class ForecastDto {

    @SerializedName("dt_txt")
    private String date;
    private MainDto main;
    @SerializedName("weather")
    private List<WeatherDto> weatherDto;

    public String getDate() {
        return date;
    }

    public void setDate(final String date) {
        this.date = date;
    }

    public MainDto getMain() {
        return main;
    }

    public void setMain(final MainDto main) {
        this.main = main;
    }

    public List<WeatherDto> getWeatherDto() {
        return weatherDto;
    }

    public void setWeatherDto(final List<WeatherDto> weatherDto) {
        this.weatherDto = weatherDto;
    }
}
