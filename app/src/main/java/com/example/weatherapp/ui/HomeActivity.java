package com.example.weatherapp.ui;

import android.widget.Toast;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import com.example.weatherapp.Provider;
import com.example.weatherapp.R;
import com.example.weatherapp.domain.ForecastDto;
import com.example.weatherapp.repository.ForecastCallback;
import com.example.weatherapp.repository.ForecastRepository;
import java.util.List;

public class HomeActivity extends AppCompatActivity implements ForecastCallback {

    private static final String LOCATION = "location";

    private ForecastAdapter adapter;
    private ForecastRepository repository;
    @Nullable
    private String location;

    @Override
    protected void onCreate(final Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        setSupportActionBar(findViewById(R.id.toolbar));

        adapter = new ForecastAdapter();
        final LinearLayoutManager linearLayoutManager = new LinearLayoutManager(this);
        final RecyclerView recyclerView = findViewById(R.id.recycler);
        recyclerView.setLayoutManager(linearLayoutManager);
        recyclerView.setAdapter(adapter);

        repository = new ForecastRepository(new Provider().provideForecastApi());
        repository.setCallback(this);
    }

    @Override
    protected void onRestoreInstanceState(final Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);
        location = savedInstanceState.getString(LOCATION);
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (location == null || location.isEmpty()) {
            repository.getForecast(location);
        }
    }

    @Override
    protected void onStop() {
        super.onStop();
        repository.dispose();
    }

    @Override
    protected void onSaveInstanceState(@NonNull final Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putString(LOCATION, location);
    }

    @Override
    public void onSuccess(final List<ForecastDto> forecastList) {
        adapter.setforecastList(forecastList);
    }

    @Override
    public void onError() {
        Toast.makeText(getApplicationContext(), "Hubo un error", Toast.LENGTH_SHORT).show();
    }
}