package com.example.weatherapp.ui;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import com.example.weatherapp.IconHelper;
import com.example.weatherapp.R;
import com.example.weatherapp.domain.ForecastDto;
import com.example.weatherapp.domain.MainDto;
import com.squareup.picasso.Picasso;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

class ForecastAdapter extends RecyclerView.Adapter<ForecastAdapter.ViewHolder> {

    private List<ForecastDto> forecastList = Collections.emptyList();

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull final ViewGroup viewGroup, final int i) {
        final View view = LayoutInflater.from(
            viewGroup.getContext()).inflate(R.layout.forecast_item, viewGroup, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull final ViewHolder viewHolder, final int i) {
        final ForecastDto forecast = forecastList.get(i);
        final MainDto mainDto = forecast.getMain();
        final Context context = viewHolder.itemView.getContext().getApplicationContext();

        viewHolder.getDay().setText(forecast.getDate());
        viewHolder.getTemperature().setText(
            context.getString(R.string.temp_celsius, String.valueOf(mainDto.getTemp())));
        viewHolder.getMaxTemperature().setText(
            context.getString(R.string.max_temp_celsius, String.valueOf(mainDto.getTempMax())));
        viewHolder.getMinTemperature().setText(
            context.getString(R.string.min_temp_celsius, String.valueOf(mainDto.getTempMin())));

        final int weatherId = forecast.getWeatherDto().get(0).getId();

        Picasso
            .with(context)
            .load(IconHelper.getResourceUri(weatherId))
            .fit()
            .centerInside()
            .into(viewHolder.getImage());
    }

    void setforecastList(final List<ForecastDto> forecastList) {
        this.forecastList = new ArrayList<>(forecastList);
        notifyDataSetChanged();
    }

    @Override
    public int getItemCount() {
        return forecastList.size();
    }

    static class ViewHolder extends RecyclerView.ViewHolder {

        private final TextView day;
        private final ImageView image;
        private final TextView temperature;
        private final TextView maxTemperature;
        private final TextView minTemperature;

        ViewHolder(@NonNull final View itemView) {
            super(itemView);
            day = itemView.findViewById(R.id.day);
            image = itemView.findViewById(R.id.image);
            temperature = itemView.findViewById(R.id.temperature);
            maxTemperature = itemView.findViewById(R.id.maxTemperature);
            minTemperature = itemView.findViewById(R.id.minTemperature);
        }

        TextView getDay() {
            return day;
        }

        ImageView getImage() {
            return image;
        }

        TextView getTemperature() {
            return temperature;
        }

        TextView getMaxTemperature() {
            return maxTemperature;
        }

        TextView getMinTemperature() {
            return minTemperature;
        }
    }
}