package com.example.weatherapp;

import androidx.annotation.NonNull;

public class IconHelper {

    @NonNull
    private static String getResourceId(final int weatherId) {
        if (weatherId < 299) {
            return "11d";
        } else if (weatherId < 399) {
            return "09d";
        } else if (weatherId < 599) {
            return "10d";
        } else if (weatherId < 699) {
            return "13d";
        } else if (weatherId < 799) {
            return "50d";
        } else if (weatherId == 800) {
            return "01d";
        } else {
            return "02d";
        }
    }

    @NonNull
    public static String getResourceUri(final int weatherId) {
        return "http://openweathermap.org/img/wn/" + getResourceId(weatherId) + "@2x.png";
    }
}
